from time import time

import tensorflow as tf
from keras.preprocessing import image
from tensorflow import keras as k
import numpy as np

from keras_preprocessing.image import ImageDataGenerator
# from createPersonsDataset import train_dir, valid_dir


train_dir = 'dataset/train'
valid_dir = 'dataset/valid'


model = None

base_model = k.applications.VGG16(include_top=False, weights='imagenet', input_shape=(150, 150, 3))
base_model.trainable = False
set_trainable = False
for layer in base_model.layers:
    if layer.name == 'block5_conv1':
        set_trainable = True
    if set_trainable:
        layer.trainable = True
    else:
        layer.trainable = False
model = k.Sequential()
model.add(base_model)
model.add(k.layers.Flatten())
model.add(k.layers.Dense(512, activation='relu'))
model.add(k.layers.Dropout(0.5))
model.add(k.layers.Dense(4, activation='sigmoid'))
model.summary()

# tensorboard = k.callbacks.TensorBoard(log_dir="logs_person/{}".format(time()))

model.compile(optimizer=k.optimizers.SGD(),
              loss=k.losses.binary_crossentropy,
              metrics=['accuracy'])

# train_datagen = ImageDataGenerator(
#                                    horizontal_flip=True)
#
# test_datagen = ImageDataGenerator(rescale=1. / 255)
#
# train_gen = train_datagen.flow_from_directory(train_dir, target_size=(150, 150), batch_size=20,
#                                               class_mode='categorical',
#                                               classes=['blur', 'brightness_dark', 'brightness_light', 'origin'])
#
# valid_gen = train_datagen.flow_from_directory(valid_dir, target_size=(150, 150), batch_size=20,
#                                               class_mode='categorical',
#                                               classes=['blur', 'brightness_dark', 'brightness_light', 'origin'])
#
# history = model.fit_generator(
#     train_gen,
#     steps_per_epoch=100,
#     epochs=10,
#     validation_data=valid_gen,
#     validation_steps=50,
#     callbacks=[tensorboard])
#
# model.save('photo_nn.h5')

img = image.load_img('dataset/train/blur/5c182713c0451bfbdd3090edc658bc48b5e96f3c.jpeg', target_size=(150, 150))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.

print(model.predicrt([img_tensor]))