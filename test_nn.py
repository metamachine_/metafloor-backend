import tensorflow as tf
import tensorflow.keras as k
from keras.preprocessing import image
import numpy as np
model = tf.keras.models.load_model('photo_nn.h5')
model.summary()
print(type(model))

img = image.load_img('dataset/valid/origin/5efae4928743f723006d7967d83f4e66e7dff7c4.jpeg', target_size=(150, 150))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.

print(model.predict(img_tensor))
print([round(x, 3) for x in model.predict(img_tensor)[0]])

