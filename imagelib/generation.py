import numpy as np
import cv2
import os

BASE_DIR = os.getcwd()
DATASET_DIR = os.path.join(BASE_DIR, 'dataset')

ORIGIN_DIR = os.path.join(DATASET_DIR, 'origin')

BRIGHTNESS_DARK_DIR = os.path.join(DATASET_DIR, 'brightness_dark')
BRIGHTNESS_LIGHT_DIR = os.path.join(DATASET_DIR, 'brightness_light')

BLUR_DIR = os.path.join(DATASET_DIR, 'blur')


for dir in (BRIGHTNESS_LIGHT_DIR, BRIGHTNESS_DARK_DIR, BLUR_DIR):
    if not os.path.exists(dir):
        os.mkdir(dir)


class Image:
    def __init__(self, path):
        self.name = os.path.basename(path)
        self.image = cv2.imread(path, -1)


def brightness(img):
    filename, extension = os.path.splitext(img.name)

    norm_image = cv2.resize(cv2.normalize(img.image, None, 0, 98, cv2.NORM_MINMAX), (300, 300))
    cv2.imwrite(os.path.join(BRIGHTNESS_DARK_DIR, filename + '_dark' + extension), norm_image)
    norm_image = cv2.resize(cv2.normalize(img.image, None, 255, 76, cv2.NORM_MINMAX), (300, 300))
    cv2.imwrite(os.path.join(BRIGHTNESS_LIGHT_DIR, filename + '_bright' + extension), norm_image)


def blur_image(img, kernel=(31, 31)):
    output = cv2.resize(cv2.GaussianBlur(img.image, kernel, 0), (300, 300))
    cv2.imwrite(os.path.join(BLUR_DIR, img.name), output)


def equalizer_back(img):
    img_yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)

    # equalize the histogram of the Y channel
    img_yuv[:, :, 0] = cv2.equalizeHist(img_yuv[:, :, 0])

    # convert the YUV image back to RGB format
    norm_image = cv2.cvtColor(img_yuv, cv2.COLOR_YUV2BGR)
    return norm_image


def build_dataset():
    for dirpath, dirnames, filenames in os.walk(ORIGIN_DIR):
        for f in filenames:
            print(f)
            img = Image(os.path.join(dirpath, f))
            blur_image(img)
            brightness(img)

