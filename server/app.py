import sys
from os.path import dirname

from flask import Flask
from flask_script import Manager
from flask_cors import CORS


sys.path.append(dirname(__file__))
app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

manager = Manager(app)
app.config.from_object('config.DevelopConfig')

from . import views
