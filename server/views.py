from ssd import detect_person
from .app import app
from flask import Response, request, jsonify, current_app
import numpy as np
import cv2
from imagelib.generation import equalizer_back
import base64



@app.route('/evaluation', methods=['POST'])
def evaluation():
    if 'file' not in request.files:
        return Response('Need file.')

    img = cv2.imdecode(np.fromstring(request.files['file'].read(), np.uint8), 3)
    img, errors = detect_person(img)
    rel, buffer = cv2.imencode('.jpg', equalizer_back(img))
    i = base64.b64encode(buffer)
    result = {
        'correction': str(i)[2:-1],
        'errors': errors
    }
    print(result)
    print(jsonify(result))
    return jsonify(result)
