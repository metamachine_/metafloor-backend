import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    ERROR_DATA = [
        {
            'Размытость': ''
        },
        {
            'Затемнение': ''
        },
        {
            'Засветление': ''
        }
    ]


class ProductionConfig(Config):
    DEBUG = False


class DevelopConfig(Config):
    DEBUG = True