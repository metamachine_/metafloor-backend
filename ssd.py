from matplotlib import pyplot as plt
from keras import backend as K
from tensorflow import keras
from keras.optimizers import Adam
from keras.preprocessing import image
import numpy as np
from ssd_keras.models.keras_ssd300 import ssd_300
from keras_loss_function.keras_ssd_loss import SSDLoss
from imageio import imread
import cv2

K.clear_session()  # Clear previous models from memory.

model = ssd_300(image_size=(300, 300, 3),
                n_classes=20,
                mode='inference',
                l2_regularization=0.0005,
                scales=[0.1, 0.2, 0.37, 0.54, 0.71, 0.88, 1.05],
                aspect_ratios_per_layer=[[1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5, 3.0, 1.0 / 3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0 / 3.0],
                                         [1.0, 2.0, 0.5, 3.0, 1.0 / 3.0],
                                         [1.0, 2.0, 0.5],
                                         [1.0, 2.0, 0.5]],
                two_boxes_for_ar1=True,
                steps=[8, 16, 32, 64, 100, 300],
                offsets=[0.5, 0.5, 0.5, 0.5, 0.5, 0.5],
                clip_boxes=False,
                variances=[0.1, 0.1, 0.2, 0.2],
                normalize_coords=True,
                subtract_mean=[123, 117, 104],
                swap_channels=[2, 1, 0],
                confidence_thresh=0.5,
                iou_threshold=0.45,
                top_k=200,
                nms_max_output_size=400)

# TODO: Set the path of the trained weights.
weights_path = 'VGG_VOC0712_SSD_300x300_iter_120000.h5'

model.load_weights(weights_path, by_name=True)

adam = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)

ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)

model.compile(optimizer=adam, loss=ssd_loss.compute_loss)

# orig_images = []  # Store the images here.
# Store resized versions of the images here.

# We'll only load one image in this example.
# TODO: CHANGE IMG
img_path = 'child_room.jpg'


# orig_images.append(cv2.imread(img_path))


def detect_person(img):
    confidence_threshold = 0.5
    orig_images = []

    orig_images.append(img)
    input_images = []
    input_img = cv2.resize(orig_images[0], (300, 300))
    input_img = keras.preprocessing.image.img_to_array(input_img)
    input_images.append(input_img)
    input_images = np.array(input_images)
    y_pred = model.predict(input_images)
    y_pred_thresh = [y_pred[k][y_pred[k, :, 1] > confidence_threshold] for k in range(y_pred.shape[0])]
    np.set_printoptions(precision=2, suppress=True, linewidth=90)
    print("Predicted boxes:\n")
    print('   class   conf xmin   ymin   xmax   ymax')
    print(y_pred_thresh[0])
    # colors = plt.cm.hsv(np.linspace(0, 1, 21)).tolist()
    classes = ['background',
               'aeroplane', 'bicycle', 'bird', 'boat',
               'bottle', 'bus', 'car', 'cat',
               'chair', 'cow', 'diningtable', 'dog',
               'horse', 'motorbike', 'person', 'pottedplant',
               'sheep', 'sofa', 'train', 'tvmonitor']
    person = False
    for box in y_pred_thresh[0]:
        if int(box[0]) == 15:
            xmin = box[2] * orig_images[0].shape[1] / 300
            ymin = box[3] * orig_images[0].shape[0] / 300
            xmax = box[4] * orig_images[0].shape[1] / 300
            ymax = box[5] * orig_images[0].shape[0] / 300
            person = True
            # label = '{}: {:.2f}'.format(classes[int(box[0])], box[1])
            cv2.rectangle(img, (int(xmax), int(ymax)), (int(xmin), int(ymin)), (0, 255, 0), 4)
        # cv2.putText(img, label, (int(xmin), int(ymin)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)
    return orig_images[0], person


detect_person(cv2.imread(img_path))
# orig_images.append(imread(img_path))
# img = image.load_img(img_path, target_size=(300, 300))
# img = image.img_to_array(img)
# input_images.append(img)
# input_images = np.array(input_images)

# y_pred = model.predict(input_images)

# confidence_threshold = 0.5
#
# y_pred_thresh = [y_pred[k][y_pred[k, :, 1] > confidence_threshold] for k in range(y_pred.shape[0])]

# np.set_printoptions(precision=2, suppress=True, linewidth=90)
# print("Predicted boxes:\n")
# print('   class   conf xmin   ymin   xmax   ymax')
# print(y_pred_thresh[0])

# Display the image and draw the predicted boxes onto it.

# Set the colors for the bounding boxes
# colors = plt.cm.hsv(np.linspace(0, 1, 21)).tolist()
# classes = ['background',
#            'aeroplane', 'bicycle', 'bird', 'boat',
#            'bottle', 'bus', 'car', 'cat',
#            'chair', 'cow', 'diningtable', 'dog',
#            'horse', 'motorbike', 'person', 'pottedplant',
#            'sheep', 'sofa', 'train', 'tvmonitor']

# plt.figure(figsize=(20, 12))
# plt.imshow(orig_images[0])

# current_axis = plt.gca()

# for box in y_pred_thresh[0]:
#     # Transform the predicted bounding boxes for the 300x300 image to the original image dimensions.
#     xmin = box[2] * orig_images[0].shape[1] / 300
#     ymin = box[3] * orig_images[0].shape[0] / 300
#     xmax = box[4] * orig_images[0].shape[1] / 300
#     ymax = box[5] * orig_images[0].shape[0] / 300
#     color = colors[int(box[0])]
#     label = '{}: {:.2f}'.format(classes[int(box[0])], box[1])
#     current_axis.add_patch(plt.Rectangle((xmin, ymin), xmax - xmin, ymax - ymin, color=color, fill=False, linewidth=2))
#     current_axis.text(xmin, ymin, label, size='x-large', color='white', bbox={'facecolor': color, 'alpha': 1.0})
#
# plt.show()
